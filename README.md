# Persian Calendar Events Scraper

This is just a simple attempt to get events of the day using `http://time.ir` website. 

To use this just add this line to your ruby codes (do not forget to include `.rb` file to your directory.) 

```ruby 

require './events-scraper.rb' 
``` 

Then, just call this function for getting information of a month or a certain day : 

```ruby 

scraper("1398", "11", "22")
scraper("1395", "5")

``` 

## Test

Terminal:
```
ruby test.rb
```
This save event in text file.